import React, { Component, PureComponent, useState } from 'react';
import { SafeAreaView, ScrollView, Dimensions, View, Text, StyleSheet, Image, TextInput } from 'react-native';
//import { rootCertificates } from 'tls';
import Carousel from 'react-native-snap-carousel';

export async function getData() {
    var res = await fetch("https://api.themoviedb.org/3/trending/all/day?api_key=7e718bbe884ff492360be457092aa3fb")
        .then((resp) => resp.json())
        .then((data) => { return data; });

    console.log("Data");
    console.log(res);
    return res;

}
export async function getDataUpcomming() {
    var res = await fetch("https://api.themoviedb.org/3/movie/popular?api_key=7e718bbe884ff492360be457092aa3fb&language=en-US&page=1")
        .then((resp) => resp.json())
        .then((data) => { return data; });

    console.log("Data");
    console.log(res);
    return res;

}
const screenWidth = Dimensions.get('window').width;

export default class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            results: [],
            resultsUpComming: [],
            activeIndex: 0,
            carouselItems: [
                {
                    title: "Item 1",
                    text: "Text 1",
                },
                {
                    title: "Item 2",
                    text: "Text 2",
                },
                {
                    title: "Item 3",
                    text: "Text 3",
                },
                {
                    title: "Item 4",
                    text: "Text 4",
                },
                {
                    title: "Item 5",
                    text: "Text 5",
                },
            ]
        }
    }

    componentDidMount() {
        this.loadApiData();
        this.loadApiDataUpcomming();
    }

    loadApiData = async () => {
        var data = await getData();
        console.log("api data");
        console.log(data);

        this.setState({ results: data.results });
    }

    loadApiDataUpcomming = async () => {
        var data = await getDataUpcomming();
        console.log("api data");
        console.log(data);

        this.setState({ resultsUpComming: data.results });
    }

    _renderItem({ item, index }) {
        return (
            <View style={{
                backgroundColor: '#333',
                borderRadius: 5,
                //height: 250,
                padding: 10,
                marginLeft: 0,
                marginRight: 0,
            }}>
                <View style={[styles.GridItem]}>
                    <View style={[styles.GridImgWrp]}>
                        <Image style={styles.GridImg} source={require('./src/images/images.jpg')} />
                    </View>
                    <View style={[styles.GridContent]}>
                        <Text style={[styles.GridTitle]}>{item.title}</Text>                       
                    </View>
                    <View style={[styles.GridFooter]}>
                        <Text style={{ color: '#fff' }}>{item.release_date} </Text>
                        {/*<Text style={{ color: '#fff' }}>6:00 pm</Text>*/}
                    </View>
                </View>  
            </View>

        )
    }

    results = () => {
        return this.state.results.map((data) => {
            return (
                <View style={[styles.ListWrp]}>
                    <View style={[styles.ListItem]}>
                        <View style={[styles.ListLeft]}>
                            <Image style={styles.Img} source={require('./src/images/images.jpg')} />
                        </View>                         
                        <View style={[styles.ListRight]}>
                            <Text style={[styles.ListTitle]}>{data.title}</Text>
                            <View style={[styles.ListFooter]}>
                                <Text style={{ width: '50%', color: '#fff' }}>{data.release_date}</Text>
                                <Text style={{ width: '50%', color: '#fff', textAlign: "right", }}>{data.media_type}</Text>
                            </View>
                        </View>

                    </View>
                </View>
            );
        });
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.scrollView}>
                    {/*<View style={[styles.PageContainer]}></View>*/}
                    <View style={[styles.TitleWrp]}>
                        <Text style={[styles.Title]}>Trending movies</Text>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', marginBottom:30, }}>
                        <Carousel
                            autoplay={true}
                            loop={true}
                            layout={"default"}
                            ref={ref => this.carousel = ref}
                            data={this.state.resultsUpComming}
                            sliderWidth={screenWidth / 100 * 89}
                            itemWidth={screenWidth / 100 * 50}
                            renderItem={this._renderItem}
                            onSnapToItem={index => this.setState({ activeIndex: index })} />
                    </View>
                    <View style={[styles.TitleWrp]}>
                        <Text style={[styles.Title]}>Popular Movies</Text>
                    </View>
                    <View>
                        {this.results()}
                    </View>

                    {/*<View style={[styles.ListWrp]}>
                        <View style={[styles.ListItem]}>
                            <View style={[styles.ListLeft]}>
                                <Image style={styles.Img} source={require('./src/images/images.jpg')} />
                            </View>
                            <View style={[styles.ListRight]}>
                                <Text style={[styles.ListTitle]}>Fast & Furious</Text>
                                <View style={[styles.ListFooter]}>
                                    <Text>Bahrain </Text>
                                    <Text>6:00 pm</Text>
                                </View>
                            </View>
                        </View>                        
                    </View>*/}


                </ScrollView>
            </SafeAreaView>

        );
    }
}

const styles = StyleSheet.create({
    text: {
        fontSize: 42,
    },
    container: {
        flex: 1,
        //paddingTop: StatusBar.currentHeight,        
    },
    scrollView: {
        //backgroundColor: 'pink',
        marginHorizontal: 0,
        padding: 15,
    },
    //PageContainer: {
    //    //flex: 1,
    //    padding: 0,
    //    backgroundColor: '#ccc',
    //},
    TitleWrp: {
        flex: 0.8,
        backgroundColor: '#ccc',
        height: 40,
        textAlign: "center",
        justifyContent: "center",
        borderRadius: 5,
        marginBottom: 30,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 5.27,
        elevation: 6,
    },
    Title: {
        //backgroundColor: '#000',
        fontSize: 18,
        color: '#000',
        textAlign: "center",
    },
    ListWrp: {
        flex: 1,
        width: '100%',
    },
    ListItem: {
        backgroundColor: '#80adf5',
        width: '100%',
        flexDirection: "row",
        borderRadius: 15,
        marginBottom: 15,
        overflow: "hidden",

        shadowColor: "#ccc",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.54,
        shadowRadius: 5.27,
        elevation: 6,
    },
    ListLeft: {
        width: '20%',
        backgroundColor: '#82b4f3',
    },
    ListRight: {
        width: '80%',
        //flex:0.3,
        padding: 15,
        alignContent: "space-around",
        //alignItems: "s",
        justifyContent: "space-around",
    },
    ListTitle: {
        fontSize: 14,
        marginBottom: 10,
        //marginTop: 10,
        color: '#fff',
    },
    Img: {
        resizeMode: "cover",
        height: 100,
        width: '100%',
    },
    ListFooter: {
        width: '100%',
        flexDirection: "row",
        //backgroundColor:'#000',
    },
    GridImgWrp: {
        backgroundColor: '#000',
    },
    GridImg: {
        width: '100%',
        height: 100,
        resizeMode: "cover",
    },
    GridContent: {
        width: '100%',
    },
    GridTitle: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 14,
        color: '#fff',
    }
});